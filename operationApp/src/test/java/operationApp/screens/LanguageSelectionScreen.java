package operationApp.screens;

import org.openqa.selenium.By;

import operationApp.base.BaseUIPage;
import operationApp.base.Hindi_AppConstants;

public class LanguageSelectionScreen extends BaseUIPage {

	boolean flag = false;

	// public void checkLanguageSelectionScreenAppearing() {
	// isElementPresent(By.id(Hindi_AppConstants.SELECT_LANGUAGE_TEXT));
	// String selectLanguageText =
	// mDriver.findElement(By.id(Hindi_AppConstants.SELECT_LANGUAGE_TEXT)).getText();
	//
	// if
	// (selectLanguageText.equals(Hindi_AppConstants.SELECT_LANGUAGE_APPEARING_TEXT))
	// {
	// flag = true;
	// } else {
	// flag = false;
	// }
	// }

	public void clickHindiLanguageButton() {
		mDriver.findElement(By.id(Hindi_AppConstants.SELECT_LANGUAGE)).click();
	}

}
