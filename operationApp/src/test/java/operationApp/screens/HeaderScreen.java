package operationApp.screens;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import operationApp.base.BaseUIPage;
import operationApp.base.Hindi_AppConstants;

public class HeaderScreen extends BaseUIPage {

	public boolean isDesiredScreenPresent(String screenName) {
		String finalScreenNameLocator = Hindi_AppConstants.SCREEN_HEADER_LABEL_FIRST + screenName
				+ Hindi_AppConstants.SCREEN_HEADER_LABEL_END;
		By object_locator = By.xpath(finalScreenNameLocator);
		boolean objectSearchResult = isElementPresent(object_locator);
		return objectSearchResult;
	}

	public void clickCreateFarmerButton() {
		mDriver.findElement(Hindi_AppConstants.CREATE_NEW_FARMER_ICON).click();
	}

	public void clickSyncButton() {
		mDriver.findElement(Hindi_AppConstants.SYNC_ICON).click();
		waitforElementClickable(10, Hindi_AppConstants.SYNC_ICON);
	}

	public void clickSearchButton() {
		mDriver.findElement(Hindi_AppConstants.SEARCH_ICON).click();
	}

	public void enterSearchField(String searchData) {
		WebElement searchField = mDriver.findElement(Hindi_AppConstants.SEARCH_TEXT_FIELD);
		searchField.clear();
		searchField.sendKeys(searchData);
	}

}
