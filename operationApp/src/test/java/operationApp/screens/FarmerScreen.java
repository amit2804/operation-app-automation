package operationApp.screens;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import operationApp.base.BaseUIPage;
import operationApp.base.Hindi_AppConstants;
import operationApp.utils.TestUtils;

public class FarmerScreen extends BaseUIPage {
	public static String FARMER_NAME = TestUtils.generateRandomName(10);
	public static String FARM_NAME = TestUtils.generateRandomName(4);
	public static String FARMER_NUMBER = TestUtils.generateRandomNumber(10);

	public static String enterFarmerDetails(String fatherName, String farmerAddress, String villageName)
			throws InterruptedException {

		String finalVillageName = Hindi_AppConstants.SELECT_VILLAGE_START + villageName
				+ Hindi_AppConstants.SELECT_VILLAGE_END;
		mDriver.findElement(Hindi_AppConstants.FARMER_FIRST_NAME).sendKeys(FARMER_NAME);
		mDriver.findElement(Hindi_AppConstants.FARMER_LAST_NAME).sendKeys("Test");
		mDriver.findElement(Hindi_AppConstants.FARMER_FATHER_NAME).sendKeys("Test Dad");
		mDriver.findElement(Hindi_AppConstants.FARMER_PHONE_NUMBER).sendKeys(FARMER_NUMBER);
		mDriver.findElement(Hindi_AppConstants.FARMER_ADDRESS).sendKeys(farmerAddress);
		pressDeviceBackButton();
		mDriver.findElement(Hindi_AppConstants.FARMER_VILLAGE).click();
		Thread.sleep(2000L);
		mDriver.findElement(By.xpath(finalVillageName)).click();
		mDriver.findElement(Hindi_AppConstants.FARMER_DETAILS_COMPLETE_BUTTON).click();
		return FARMER_NAME;
	}

	public static String addFarm(String farmName, String farmArea, String villageName, String farmAccessiblity)
			throws InterruptedException {
		String finalVillageName = Hindi_AppConstants.SELECT_VILLAGE_START + villageName
				+ Hindi_AppConstants.SELECT_VILLAGE_END;

		String finalFarmAccessibility = Hindi_AppConstants.SELECT_VILLAGE_START + farmAccessiblity
				+ Hindi_AppConstants.SELECT_VILLAGE_END;

		mDriver.findElement(Hindi_AppConstants.FARM_DETAIL_LABEL).click();

		mDriver.findElement(Hindi_AppConstants.ADD_FARM_BUTTON).click();

		WebElement enterFarmName = mDriver.findElement(Hindi_AppConstants.FARM_NAME_TEXT_FIELD);
		enterFarmName.clear();
		enterFarmName.sendKeys(farmName);

		mDriver.findElement(Hindi_AppConstants.FARM_VILLAGE_DROPDOWN).click();
		Thread.sleep(2000L);
		mDriver.findElement(By.xpath(finalVillageName)).click();

		WebElement enterFarmArea = mDriver.findElement(Hindi_AppConstants.FARM_AREA_TEXT_FIELD);
		enterFarmArea.clear();
		enterFarmArea.sendKeys(farmArea);

		mDriver.findElement(Hindi_AppConstants.FARM_SOIL_TYPE_DROPDOWN).click();
		Thread.sleep(2000L);
		mDriver.findElement(Hindi_AppConstants.FARM_SOIL_TYPE).click();

		mDriver.findElement(Hindi_AppConstants.FARM_ACCESSIBILITY_DROPDOWN).click();
		Thread.sleep(2000L);
		mDriver.findElement(Hindi_AppConstants.FARM_SOIL_TYPE).click();
		
		mDriver.findElement(Hindi_AppConstants.FARMER_DETAILS_COMPLETE_BUTTON).click();
		return FARM_NAME;
	}

	public void clickFarmerAfterSearch(String farmerNme) {
		String finalFarmerNameDisplay = Hindi_AppConstants.FARMER_NAME_DISPLAY_START + farmerNme
				+ Hindi_AppConstants.FARMER_NAME_DISPLAY_END;
		mDriver.findElement(By.xpath(finalFarmerNameDisplay)).click();
	}
}
