package operationApp.screens;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import operationApp.base.BaseUIPage;
import operationApp.base.Hindi_AppConstants;

public class LoginScreen extends BaseUIPage {

	boolean flag = false;

	public boolean isSelectLanguageScreenAppearing() {
		By object_locator = Hindi_AppConstants.SELECT_LANGUAGE_TEXT;
		boolean objectSearchResult = isElementPresent(object_locator);
		return objectSearchResult;
	}

	public boolean isOtpScreenAppearing() {
		By object_locator = Hindi_AppConstants.RESEND_OTP_BUTTON;
		boolean objectSearchResult = isElementPresent(object_locator);
		return objectSearchResult;
	}

	public void checkLanguageSelectionScreenAppearing() {
		String selectLanguageText = mDriver.findElement(Hindi_AppConstants.SELECT_LANGUAGE_TEXT).getText();
		if (selectLanguageText.equals(Hindi_AppConstants.SELECT_LANGUAGE_APPEARING_TEXT)) {
			flag = true;
		} else {
			flag = false;
		}
	}

	public void clickHindiLanguageButton() {
		mDriver.findElement(By.id(Hindi_AppConstants.SELECT_LANGUAGE)).click();
	}

	public void checkPhoneNumberScreenAppearing() {
		String selectLanguageText = mDriver.findElement(Hindi_AppConstants.SELECT_LANGUAGE_TEXT).getText();
		if (selectLanguageText.equals(Hindi_AppConstants.ENTER_PHONE_NUMBER_TEXT)) {
			flag = true;
		} else {
			flag = false;
		}
	}

	public void clickHindi() {
		mDriver.findElementByAccessibilityId(Hindi_AppConstants.LOGIN_BUTTON).click();
	}

	public void enterPhoneNumber(String phoneNumber) {
		WebElement phoneNumberField = mDriver.findElement(By.id(Hindi_AppConstants.ENTER_PHONE_NUMBER_TEXT_FIELD));
		phoneNumberField.clear();
		phoneNumberField.sendKeys(phoneNumber);
		mDriver.findElement(By.id(Hindi_AppConstants.LOGIN_BUTTON)).click();
	}

	public void enterOtpNumber(String otpNumber) {
		WebElement otpNumberField = mDriver.findElement(Hindi_AppConstants.ENTER_OTP_TEXT_FIELD);
		otpNumberField.clear();
		otpNumberField.sendKeys(otpNumber);
		mDriver.findElement(Hindi_AppConstants.LOGIN_BUTTON_OTP_SCREEN).click();
	}

	public void selectCenterName(String centerName) throws InterruptedException {
		WebElement centerSelectField = mDriver.findElement(Hindi_AppConstants.SELECT_CENTER);
		// centerSelectField.clear();
		centerSelectField.sendKeys(centerName);
		Thread.sleep(3000L);
		pressDeviceBackButton();
		mDriver.findElement(Hindi_AppConstants.NEXT_BUTTON_CENTER).click();
	}

	public boolean isSetPinScreenAppearing() {
		waitforElement(10, Hindi_AppConstants.ENTER_PIN_TEXT_FIELD);
		By object_locator = Hindi_AppConstants.ENTER_PIN_TEXT_FIELD;
		boolean objectSearchResult = isElementPresent(object_locator);
		return objectSearchResult;
	}

	public void setUserPin(String userPin) {
		WebElement setUserPinField = mDriver.findElement(Hindi_AppConstants.ENTER_PIN_TEXT_FIELD);
		setUserPinField.clear();
		setUserPinField.sendKeys(userPin);
		WebElement confirmUserPinField = mDriver.findElement(Hindi_AppConstants.CONFIRM_PIN_TEXT_FIELD);
		confirmUserPinField.clear();
		confirmUserPinField.sendKeys(userPin);
		mDriver.findElement(Hindi_AppConstants.NEXT_BUTTON_PIN_SCREEN).click();
	}

	public void enterUserPin(String userPin) {
		WebElement setUserPinField = mDriver.findElement(Hindi_AppConstants.ENTER_PIN);
		setUserPinField.clear();
		setUserPinField.sendKeys(userPin);
		mDriver.findElement(Hindi_AppConstants.PIN_SUBMIT_BUTTON).click();
	}
}
