package operationApp.screens;

import org.openqa.selenium.By;

import operationApp.base.BaseUIPage;
import operationApp.base.Hindi_AppConstants;

public class SidePanelScreen extends BaseUIPage {

	public void openNavigationDrawer() {
		mDriver.findElement(Hindi_AppConstants.MENU_ICON_BUTTON).click();
	}

	public void clickOptionUnderNavigationDrawer(String optionName) {
		String finalLabelLocator = Hindi_AppConstants.LABELS_SIDEPANEL_START + optionName
				+ Hindi_AppConstants.LABELS_SIDEPANEL_END;
		mDriver.findElement(By.xpath(finalLabelLocator)).click();
	}

}
