package operationApp.base;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import operationApp.screens.LanguageSelectionScreen;
import operationApp.utils.CommonUtils;
import operationApp.utils.TestUtils;

public class Set_Driver_Set_Capabilities {

	public static AndroidDriver mDriver;
	public static WebDriver wDriver;
	public static Logger log = Logger.getLogger("devpinoyLogger");
	public static String loadPropertyFile = "Hindi_operationApp.properties";
	public static String loadDataFile = "datafile.properties";
	public static LanguageSelectionScreen LS_Screen;
	public static ExtentReports reports;
	public static ExtentTest testinfo;
	public static ExtentHtmlReporter htmlReporter;
	public static Properties dataFileProperties;

	@BeforeSuite
	public void setDesiredCapabilities() throws InterruptedException, IOException {

		if (mDriver == null) {
			if (loadPropertyFile.startsWith("Hindi")) {
				log.debug("Starting on Operation App");
				CommonUtils.loadOperationAppConfigProp(loadPropertyFile);
				CommonUtils.setOperationAppCapabilities();
				mDriver = CommonUtils.getAndroidDriver();
			} else if (loadPropertyFile.startsWith("English")) {
				log.debug("Starting on Operation App as English Language");
				CommonUtils.loadOperationAppConfigProp(loadPropertyFile);
				CommonUtils.setOperationAppCapabilities();
				mDriver = CommonUtils.getAndroidDriver();
			}
			BaseUIPage bp = new BaseUIPage();
			bp.setMobileDriver(mDriver);

			htmlReporter = new ExtentHtmlReporter(new File(System.getProperty("user.dir") + "/AutomationReports.html"));
			htmlReporter.loadXMLConfig(
					new File(System.getProperty("user.dir") + "/src/test/resources/runner/extent-config.xml"));
			reports = new ExtentReports();
			reports.setSystemInfo("Environment", "Local");
			reports.setSystemInfo("App Name", "Operation App");
			reports.attachReporter(htmlReporter);
			
			dataFileProperties = new Properties();
			FileInputStream fis = new FileInputStream(
					System.getProperty("user.dir") + "/src/test/resources/properties/datafile.properties");
			dataFileProperties.load(fis);
		}
	}

	public String getTestClassName(String testName) {
		String[] reqTestClassname = testName.split("\\.");
		int i = reqTestClassname.length - 1;
		System.out.println("Required Test Name : " + reqTestClassname[i]);
		return reqTestClassname[i];
	}

	@BeforeMethod
	public void register(Method method) {
		String testName = method.getName();
		testinfo = reports.createTest(testName);
	}

	@AfterMethod
	public void afterM(ITestResult result) {

		if (result.getStatus() == ITestResult.SUCCESS) {
			testinfo.log(Status.PASS, "Test Method name as : " + result.getName() + "is passed");
		} else if (ITestResult.FAILURE == result.getStatus()) {
			testinfo.log(Status.PASS, "Test Method name as : " + result.getName() + "is failed");
			testinfo.log(Status.FAIL, "Test failure :" + result.getThrowable());
			TestUtils.CaptureScreenshot(result.getName());
		}
	}

	@AfterSuite
	public void tearDown() throws IOException {
		reports.flush();
		mDriver.quit();
	}

}