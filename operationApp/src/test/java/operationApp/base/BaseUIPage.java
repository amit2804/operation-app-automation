package operationApp.base;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;
import operationApp.screens.HeaderScreen;
import operationApp.utils.TestUtils;

public class BaseUIPage {

	protected static AndroidDriver mDriver;
	protected static WebDriver wDriver;
	protected static WebDriverWait wait;
	protected Hindi_AppConstants H_AC_Obj = new Hindi_AppConstants();
	protected static HeaderScreen HS_Obj = new HeaderScreen();
	protected static TestUtils TU_Obj;

	// public static DesiredCapabilities mCapabilities;

	public void setMobileDriver(AndroidDriver mDriver) {
		BaseUIPage.mDriver = mDriver;
	}

	public void setMobileDriverAndDevice(AndroidDriver mDriver) {
		setMobileDriver(mDriver);
	}

	public void setWebDriver(WebDriver wDriver) {
		this.wDriver = wDriver;
	}

	public void setWebDriverAndDevice(WebDriver wDriver) {
		setWebDriver(wDriver);
	}

	public boolean isElementPresent(By object_locator) {
		try {
			if (mDriver.findElement(object_locator) != null)
				return true;
		} catch (org.openqa.selenium.NoSuchElementException e) {
			return false;
		}
		return false;

	}

	public boolean isWebElementPresent(By object_locator) {
		if (wDriver.findElement(object_locator) != null)
			return true;
		else
			return false;
	}

	public void initWebsiteDriver() {
		System.setProperty("webdriver.firefox.bin", "/Applications/Firefox.app/Contents/MacOS/firefox-bin");
		wDriver = new FirefoxDriver();
		// webDriver = new ChromeDriver();
		wDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	public void closeWebsiteDriver() {
		wDriver.quit();
	}

	public void waitforElement(long duration, By locator) {

		wait = new WebDriverWait(mDriver, duration);
		wait.until(ExpectedConditions.presenceOfElementLocated(locator));

	}

	public static void waitforElementClickable(long duration, By locator) {

		wait = new WebDriverWait(mDriver, duration);
		wait.until(ExpectedConditions.elementToBeClickable(locator));

	}

	public static void pressDeviceBackButton() {
		mDriver.pressKeyCode(AndroidKeyCode.BACK);
	}
}
