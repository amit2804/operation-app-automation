package operationApp.base;

import org.openqa.selenium.By;

public class Hindi_AppConstants {

	// Language Selection Screen Elements
	public static final By SELECT_LANGUAGE_TEXT = By.id("com.em3Agri.operation.local:id/textView");

	public static final String SELECT_LANGUAGE_APPEARING_TEXT = "Select your Preferred Language";

	public static final String SELECT_LANGUAGE = "com.em3Agri.operation.local:id/btn_hindi";

	// Login Screen Elements
	public static final String ENTER_PHONE_NUMBER_TEXT = "दस अंकों का पंजीकृत मोबाइल नंबर दर्ज करें";

	public static final String ENTER_PHONE_NUMBER_TEXT_FIELD = "com.em3Agri.operation.local:id/input_number";

	public static final String LOGIN_BUTTON = "com.em3Agri.operation.local:id/btn_login";

	// Enter OTP Screen Elements
	public static final By RESEND_OTP_BUTTON = By.id("com.em3Agri.operation.local:id/btn_resend_otp");

	public static final By ENTER_OTP_TEXT_FIELD = By.id("com.em3Agri.operation.local:id/input_number");

	public static final By LOGIN_BUTTON_OTP_SCREEN = By.id("com.em3Agri.operation.local:id/btn_next");

	// Select Center Screen Elements
	public static final By SELECT_CENTER = By.id("com.em3Agri.operation.local:id/select_center");

	public static final By NEXT_BUTTON_CENTER = By.id("com.em3Agri.operation.local:id/btn_next");

	// Enter PIN Screen Elements
	public static final String ENTER_PIN_TEXT = "कृपया चार अंकों का पिन चुनें";
	public static final By ENTER_PIN_TEXT_FIELD = By.id("com.em3Agri.operation.local:id/input_pin");
	public static final By CONFIRM_PIN_TEXT_FIELD = By.id("com.em3Agri.operation.local:id/input_pin_confirm");
	public static final By NEXT_BUTTON_PIN_SCREEN = By.id("com.em3Agri.operation.local:id/btn_next");
	public static final By ENTER_PIN = By.id("com.em3Agri.operation.local:id/pin_number");
	public static final By PIN_SUBMIT_BUTTON = By.id("com.em3Agri.operation.local:id/submit_pin");

	// Header Bar Elements
	public static String SCREEN_HEADER_LABEL_FIRST = "//android.widget.TextView[@resource-id='com.em3Agri.operation.local:id/toolbar_title'and@text='";
	public static String SCREEN_HEADER_LABEL_END = "']";
	public static final By SYNC_ICON = By.id("com.em3Agri.operation.local:id/menu_sync_now");
	public static final By NOTIFICATION_ICON = By.id("com.em3Agri.operation.local:id/menu_notification");
	public static final By CREATE_NEW_SERVICE_REQUEST_ICON = By
			.id("com.em3Agri.operation.local:id/new_service_request");
	public static final By FARMER_DETAILS_COMPLETE_BUTTON = By.id("com.em3Agri.operation.local:id/menu_edit_done");
	public static final By CREATE_NEW_FARMER_ICON = By.id("com.em3Agri.operation.local:id/menu_edit_done");
	public static final By SEARCH_ICON = By.id("com.em3Agri.operation.local:id/action_search");
	public static final By SEARCH_TEXT_FIELD = By.id("com.em3Agri.operation.local:id/search_src_text");
	public static final By MENU_ICON_BUTTON = By.id("ओपन नेविगेशन ड्रॉवर");
	public static final By NAVIGATE_BACK_ICON = By.id("ऊपर नेविगेट करें");
	public static String CREATE_FARMER_SCREEN_LABEL = "किसान पंजीकरण";
	public static String ADD_FARM_SCREEN_LABEL = "खेत जोड़ें";

	// Side Panel Elements
	public static String SIDEPANEL_USERNAME_START = "//android.widget.TextView[@resource-id='com.em3Agri.operation.local:id/user_name' and @text='";
	public static String SIDEPANEL_USERNAME_END = "']";
	public static String LABELS_SIDEPANEL_START = "//android.widget.CheckedTextView[@resource-id='com.em3Agri.operation.local:id/design_menu_item_text' and @text='";
	public static String LABELS_SIDEPANEL_END = "']";
	public static String HOMEPAGE_LABEL_SIDEPANEL = "होमपेज";
	public static String FARMER_LABEL_SIDEPANEL = "किसान";
	public static String SERVICE_REQUEST_LABEL_SIDEPANEL = "सेवा अनुरोध";
	public static String JOBS_LABEL_SIDEPANEL = "जॉब्स";
	public static String MACHINE_LABEL_SIDEPANEL = "मशीन";
	public static String FUEL_LABEL_SIDEPANEL = "डीजल";
	public static String SCHEDULE_LABEL_SIDEPANEL = "कैलेंडर";
	public static String CENTER_ACCOUNT_LABEL_SIDEPANEL = "केंद्र खाता";
	public static String SPOT_BOOKING_LABEL_SIDEPANEL = "स्पॉट बुकिंग";
	public static String APPROVAL_LIST_LABEL_SIDEPANEL = "अनुमोदन सूची";
	public static String REPORT_LABEL_SIDEPANEL = "रिपोर्ट";
	public static String MEASURE_AREA_LABEL_SIDEPANEL = "खेत की नपाई";
	public static String SETTINGS_LABEL_SIDEPANEL = "सेटिंग्स";
	public static String MISC_LABEL_SIDEPANEL = "विविध";

	// FARMER SCREEN ELEMENTS
	public static final By FARMER_FIRST_NAME = By.id("com.em3Agri.operation.local:id/farmer_first_name");
	public static final By FARMER_LAST_NAME = By.id("com.em3Agri.operation.local:id/farmer_last_name");
	public static final By FARMER_FATHER_NAME = By.id("com.em3Agri.operation.local:id/farmer_father_name");
	public static final By FARMER_PHONE_NUMBER = By.id("com.em3Agri.operation.local:id/farmer_mobile_no");
	public static final By FARMER_ADDRESS = By.id("com.em3Agri.operation.local:id/farmer_address");
	public static final By FARMER_VILLAGE = By.id("com.em3Agri.operation.local:id/farmer_village");
	public static String SELECT_VILLAGE_START = "//android.widget.CheckedTextView[@resource-id='android:id/text1' and @text='";
	public static String SELECT_VILLAGE_END = "']";
	public static final By CALL_FARMER_VERIFY_BUTTON = By.id("com.em3Agri.operation.local:id/call_button");
	public static final By VERIFY_FARMER_BUTTON = By.id("com.em3Agri.operation.local:id/request_verification");
	public static final By EDIT_FARMER_DETAILS_BUTTON = By.id("com.em3Agri.operation.local:id/farmer_edit");
	public static final By FARM_DETAIL_LABEL = By.xpath("//android.widget.TextView[@text='खेत की जानकारी']");
	public static final By ADD_FARM_BUTTON = By.id("com.em3Agri.operation.local:id/add_farm");
	public static final By FARM_NAME_TEXT_FIELD = By.id("com.em3Agri.operation.local:id/farmer_farm_name");
	public static final By FARM_AREA_TEXT_FIELD = By.id("com.em3Agri.operation.local:id/farmer_farm_area");
	public static final By FARM_VILLAGE_DROPDOWN = By.id("com.em3Agri.operation.local:id/farm_village");
	public static final By FARM_SOIL_TYPE_DROPDOWN = By.id("com.em3Agri.operation.local:id/soil_type");
	public static final By FARM_ACCESSIBILITY_DROPDOWN = By.id("com.em3Agri.operation.local:id/accessibility_type");
	public static String FARMER_NAME_DISPLAY_START = "//android.widget.TextView[@resource-id='com.em3Agri.operation.local:id/first_name' and @text='";
	public static String FARMER_NAME_DISPLAY_END = "']";
	public static String FARM_ACCESSIBILITY_OPTION_NO = "नहीं";
	public static String FARM_ACCESSIBILITY_OPTION_YES = "हाँ";
	public static final By FARM_SOIL_TYPE = By
			.xpath("//android.widget.CheckedTextView[@resource-id='android:id/text1'][2]");

}
