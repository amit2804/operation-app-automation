package operationApp.utils;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import operationApp.base.Set_Driver_Set_Capabilities;

public class TestUtils extends Set_Driver_Set_Capabilities {

	/*
	 * Code Block to Capture ScreenShot only in-case of Failed TestCase.
	 */

	public static void CaptureScreenshot(String ScreenshotName) {
		try {
			TakesScreenshot ss = (TakesScreenshot) mDriver;
			File source = ss.getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(source, new File("./src/test/resources/Failed Screenshots/" + ScreenshotName + ".png"));
			System.out.println("Screen Shot Captured");
		} catch (Exception e) {
			System.out.println("Exception while taking the screenshot" + e.getMessage());
		}
	}

	public static void Screenshot(String ScreenshotName) throws IOException {
		TakesScreenshot ss = (TakesScreenshot) mDriver;
		File source = ss.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(source, new File("./Screenshots/" + ScreenshotName + ".png"));

	}

	public static String generateRandomName(int count) {
		final String ALPHA_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwzyz";
		StringBuilder builder = new StringBuilder();
		while (count-- != 0) {
			int character = (int) (Math.random() * ALPHA_STRING.length());
			builder.append(ALPHA_STRING.charAt(character));
		}
		return builder.toString();
	}

	public static String generateRandomNumber(int count) {
		final String ALPHA_STRING = "0123456789";
		StringBuilder builder = new StringBuilder();
		while (count-- != 0) {
			int character = (int) (Math.random() * ALPHA_STRING.length());
			builder.append(ALPHA_STRING.charAt(character));
		}
		return builder.toString();
	}
}
