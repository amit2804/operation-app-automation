package operationApp.testcases;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import operationApp.base.Hindi_AppConstants;
import operationApp.base.Set_Driver_Set_Capabilities;
import operationApp.screens.FarmerScreen;
import operationApp.screens.HeaderScreen;
import operationApp.screens.LoginScreen;
import operationApp.screens.SidePanelScreen;

public class AddAgentCommissionTest extends Set_Driver_Set_Capabilities {

	HeaderScreen HS_Obj = new HeaderScreen();
	FarmerScreen FS_Obj = new FarmerScreen();
	SidePanelScreen SPS_Obj = new SidePanelScreen();
	LoginScreen LS_Obj = new LoginScreen();

	@BeforeClass
	public void init() {
		LS_Obj.enterUserPin(dataFileProperties.getProperty("APP_PIN"));
	}

	@Test(priority = 0)
	public void createFarmer() throws InterruptedException {
		SPS_Obj.openNavigationDrawer();
		SPS_Obj.clickOptionUnderNavigationDrawer(Hindi_AppConstants.FARMER_LABEL_SIDEPANEL);
		HS_Obj.isDesiredScreenPresent(Hindi_AppConstants.FARMER_LABEL_SIDEPANEL);
		HS_Obj.clickCreateFarmerButton();
		HS_Obj.isDesiredScreenPresent(Hindi_AppConstants.CREATE_FARMER_SCREEN_LABEL);
		String FARMER_NAME = FS_Obj.enterFarmerDetails(dataFileProperties.getProperty("FATHER_NAME"),
				dataFileProperties.getProperty("FARMER_ADDRESS"), dataFileProperties.getProperty("VILLAGE_NAME"));
		HS_Obj.isDesiredScreenPresent(Hindi_AppConstants.FARMER_LABEL_SIDEPANEL);
		HS_Obj.clickSyncButton();
		HS_Obj.clickSearchButton();
		HS_Obj.enterSearchField(FARMER_NAME);
		FS_Obj.clickFarmerAfterSearch(FARMER_NAME);
		FS_Obj.addFarm(dataFileProperties.getProperty("FARM_NAME"), dataFileProperties.getProperty("FARM_AREA"),
				dataFileProperties.getProperty("VILLAGE_NAME"), Hindi_AppConstants.FARM_ACCESSIBILITY_OPTION_NO);
	}

}
