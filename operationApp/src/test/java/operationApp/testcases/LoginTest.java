package operationApp.testcases;

import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;

import operationApp.base.BaseUIPage;
import operationApp.base.Hindi_AppConstants;
import operationApp.base.Set_Driver_Set_Capabilities;
import operationApp.screens.LoginScreen;
import operationApp.screens.SidePanelScreen;

public class LoginTest extends Set_Driver_Set_Capabilities {

	BaseUIPage BP_Obj = new BaseUIPage();
	LoginScreen LS_Screen = new LoginScreen();
	SidePanelScreen SP_Screen = new SidePanelScreen();

	@Test(priority = 0)
	public void verifyLanguageSelectionScreenPresent() {
		testinfo.log(Status.INFO, "Select Language Test");
		LS_Screen.isSelectLanguageScreenAppearing();
		LS_Screen.checkLanguageSelectionScreenAppearing();
	}

	@Test(priority = 1)
	public void clickHindiLanguageButton() {
		testinfo.log(Status.INFO, "Click Hindi Language Button");
		LS_Screen.clickHindiLanguageButton();
	}

	@Test(priority = 2)
	public void verifyPhoneNumberScreenPresent() {
		testinfo.log(Status.INFO, "Verify Phone Number Screen Appearing");
		LS_Screen.checkPhoneNumberScreenAppearing();
	}

	@Test(priority = 3)
	public void enterUserPhoneNumber() {
		testinfo.log(Status.INFO, "Entering User Phone Number");
		LS_Screen.enterPhoneNumber(dataFileProperties.getProperty("CENTER_MANAGER_PHONENUMBER"));
	}

	@Test(priority = 4)
	public void enterOtpNumber() {
		testinfo.log(Status.INFO, "Entering OTP Number");
		LS_Screen.isOtpScreenAppearing();
		LS_Screen.enterOtpNumber(dataFileProperties.getProperty("DEFAULT_OTP"));
	}

	@Test(priority = 5)
	public void selectCenter() throws InterruptedException {
		testinfo.log(Status.INFO, "Selecting Center");
		LS_Screen.selectCenterName(dataFileProperties.getProperty("CENTER_NAME") + "," + " "
				+ dataFileProperties.getProperty("DISTRICT_NAME") + "," + " "
				+ dataFileProperties.getProperty("STATE_NAME"));

	}

	@Test(priority = 6)
	public void setUserPin() {
		testinfo.log(Status.INFO, "Setting User Pin");
		LS_Screen.isSetPinScreenAppearing();
		LS_Screen.setUserPin(dataFileProperties.getProperty("APP_PIN"));
	}

	@Test(priority = 7)
	public void clickDesiredOptionUnderSidePanel() {
		SP_Screen.openNavigationDrawer();
		SP_Screen.clickOptionUnderNavigationDrawer(Hindi_AppConstants.SERVICE_REQUEST_LABEL_SIDEPANEL);

	}

}
